provider "aws" {
    region     = "${var.AWS_REGION}"
    access_key = "AKIAXXXNTM5FJPQNUUTA"
    secret_key = "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"
}

resource "aws_key_pair" "ec2_terraform" {
    key_name   = "key_jekyll"
    public_key = file("/root/jekyll/key_jekyll.pub")
}

######################### Reseaux ##########################################

resource "aws_vpc" "vpc_group3" {
        cidr_block = "20.0.0.0/16"
        tags = {
                Name = "vpc_jekyll"
        }
 }

		#### _________VPC-Subnet_______####

resource "aws_subnet" "subnet_public1"{
        vpc_id                  =        aws_vpc.vpc_group3.id
        cidr_block              =       "20.0.121.0/24"
        availability_zone       =       "${var.AWS_REGION}a"
        map_public_ip_on_launch =       true
        tags = {
                Name = "Subnet_public1"
        }
}

resource "aws_subnet" "subnet_public2"{
        vpc_id                  =        aws_vpc.vpc_group3.id
        cidr_block              =       "20.0.122.0/24"
        availability_zone       =       "${var.AWS_REGION}b"
        map_public_ip_on_launch =       true
        tags = {
                Name = "Subnet_public2"
        }
}

resource "aws_subnet" "subnet_private1"{
        vpc_id                  =       aws_vpc.vpc_group3.id
        cidr_block              =       "20.0.103.0/24"
        availability_zone       =       "${var.AWS_REGION}a"
        map_public_ip_on_launch =        false
        tags = {
                Name = "Subnet_private1"
        }
}
resource "aws_subnet" "subnet_private2"{
        vpc_id 			= aws_vpc.vpc_group3.id
        cidr_block 		= "20.0.124.0/24"
        availability_zone 	= "${var.AWS_REGION}b"
        map_public_ip_on_launch = false
        tags = {
                Name = "Subnet_private2"
        }
}
                #### _________Gateway_______####

resource "aws_internet_gateway" "gateway_gp3" {
        
	vpc_id  =  aws_vpc.vpc_group3.id
        tags = {
                Name = "Gateway_gp3"
        }
}

resource "aws_eip" "eip_group3" {
	vpc 	   = 	true
        depends_on = 	[aws_internet_gateway.gateway_gp3]
        tags = {
                Name = "IP-jekyll"
        }
}

resource "aws_nat_gateway" "group3_nat" {
        allocation_id = aws_eip.eip_group3.id
        subnet_id     = aws_subnet.subnet_public1.id
        tags = {
                Name = "Group3_Nat"
        }
    depends_on = [aws_internet_gateway.gateway_gp3]
}

		#### _________Route_Table_______####

resource "aws_route_table" "route_table_public_group3" {
        vpc_id = aws_vpc.vpc_group3.id
        route {
                cidr_block = "0.0.0.0/0"
                gateway_id    = aws_internet_gateway.gateway_gp3.id
        }
        tags = {
                Name = "Route_table_public_group3"
        }
}
resource "aws_route_table" "route_table_private_group3" {
        vpc_id = aws_vpc.vpc_group3.id
        route {
                cidr_block = "0.0.0.0/0"
                gateway_id    = aws_nat_gateway.group3_nat.id
        }
        tags = {
                Name = "Route_table_private_group3"
        }
}

		#### _________Association_______####

resource "aws_route_table_association" "public1_group3" {
        subnet_id  = aws_subnet.subnet_public1.id
        route_table_id = aws_route_table.route_table_public_group3.id
}
resource "aws_route_table_association" "public2_group3" {
        subnet_id  = aws_subnet.subnet_public2.id
        route_table_id = aws_route_table.route_table_public_group3.id
}
resource "aws_route_table_association" "private1_group3" {
        subnet_id  = aws_subnet.subnet_private1.id
        route_table_id = aws_route_table.route_table_private_group3.id
}
resource "aws_route_table_association" "private2_group3" {
        subnet_id  = aws_subnet.subnet_private2.id
        route_table_id = aws_route_table.route_table_private_group3.id
}

	####____Load Balancer et Auto scaling AWS____####


resource "aws_security_group" "lbgroup" {
        #name = "LBG"
        vpc_id = aws_vpc.vpc_group3.id
        ingress {
                from_port   = 9002
                to_port     = 9002
                protocol    = "tcp"
                cidr_blocks = ["0.0.0.0/0"]
        }
        ingress {
                from_port = 22
                to_port = 22
                protocol = "tcp"
                cidr_blocks = ["0.0.0.0/0"]
        }
        egress {
                from_port       = 0
                to_port         = 0
                protocol        = "-1"
                cidr_blocks     = ["0.0.0.0/0"]
        }
	
 	ingress {
                from_port   = 8080
                to_port     = 8080
                protocol    = "tcp"
                cidr_blocks = ["0.0.0.0/0"]
	}

	ingress {
                from_port   = 8081
                to_port     = 8081
                protocol    = "tcp"
                cidr_blocks = ["0.0.0.0/0"]
        }


	 ingress {
                from_port   = 80
                to_port     = 80
                protocol    = "tcp"
                cidr_blocks = ["0.0.0.0/0"]
        }
	
	ingress {
                from_port   = 3306
                to_port     = 3306
                protocol    = "tcp"
                cidr_blocks = ["0.0.0.0/0"]
        }

        tags = {
                Name = "LBG"
        }
}

resource "aws_security_group" "asgroup3" {
        #name = "ASG"
        vpc_id = aws_vpc.vpc_group3.id
        ingress {
                from_port   = 9002
                to_port     = 9002
                protocol    = "tcp"
                security_groups = [aws_security_group.lbgroup.id]
         }
	 ingress {
                from_port = 22
                to_port = 22
                protocol = "tcp"
                cidr_blocks = ["0.0.0.0/0"]
        }
        egress {
                from_port       = 0
                to_port         = 0
                protocol        = "-1"
                cidr_blocks     = ["0.0.0.0/0"]
        }

	 ingress {
                from_port   = 8080
                to_port     = 8080
                protocol    = "tcp"
                cidr_blocks = ["0.0.0.0/0"]
        }

	ingress {
                from_port   = 8081
                to_port     = 8081
                protocol    = "tcp"
                cidr_blocks = ["0.0.0.0/0"]
        }  
	
	 ingress {
                from_port   = 80
                to_port     = 80
                protocol    = "tcp"
                cidr_blocks = ["0.0.0.0/0"]
        }

	 ingress {
                from_port   = 3306
                to_port     = 3306
                protocol    = "tcp"
                cidr_blocks = ["0.0.0.0/0"]
        }


         tags = {
                Name = "ASG"
         }
}



resource "aws_lb_target_group" "target_lbgroup3" {
        port = "80"
        protocol = "HTTP"
        vpc_id = aws_vpc.vpc_group3.id
        tags = {
                Name = "Target_lbgroup3"
        }
}
resource "aws_lb" "lbg_group3" {
        #name = "lbg_group3"
        load_balancer_type = "application"
        security_groups = [aws_security_group.lbgroup.id]
        subnets =[aws_subnet.subnet_public1.id , aws_subnet.subnet_public2.id]
        tags = {
                Name = "Load_Balancer_group3"
        }
}

                #### Instance AWS ####


data "aws_ami" "ubuntu_ami" {
        most_recent = true
        filter {
                name   = "name"
                values = ["ubuntu-minimal/images-testing/hvm-ssd/ubuntu-focal-daily-amd64-minimal-20211117"]
        }
                owners = ["099720109477"]
}

resource "aws_instance" "aws_group3_server" {
        #image_id = data.aws_ami.ubuntu_ami.id
        key_name      = aws_key_pair.ec2_terraform.key_name
        ami = data.aws_ami.ubuntu_ami.id
        instance_type = "t3.xlarge"
        vpc_security_group_ids = ["${aws_security_group.asgroup3.id}"]
        associate_public_ip_address = true
        subnet_id = aws_subnet.subnet_public1.id  
     
	ebs_block_device {
    		device_name = "/dev/sda1"
    		volume_size = 100
         }

	tags = {
                Name = "Jenkins-Gitlab-ci"
        }
    connection {
            type        = "ssh"
            user        = "ubuntu"
            private_key = file("/root/jekyll/key_jekyll")
            host        = self.public_ip
        }

provisioner "file" {
                source      = "/root/.ssh/id_rsa.pub"
                destination = "/tmp/keys"
}
    user_data = <<-EOF
        #!/bin/bash
	
	sudo apt update -y  && sudo apt upgrade -y
        sudo echo -e 'winner' | passwd root
 	sudo sed -i 's/#PermitRootLogin\ prohibit-password/PermitRootLogin\ yes/g' /etc/ssh/sshd_config
	sudo cat /tmp/keys >> /root/.ssh/authorized_keys	
        sudo chmod 600 /root/.ssh/authorized_keys

    EOF

    provisioner "local-exec" {
            working_dir= "/root/jekyll"
    	    command = "echo '${aws_instance.aws_group3_server.public_ip}' > /root/jekyll/ip_address.txt"
        }
}

output "public_ip" {
        value = aws_instance.aws_group3_server.public_ip
	}
