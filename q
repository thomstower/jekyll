{
    "Images": [
        {
            "Architecture": "x86_64",
            "CreationDate": "2021-11-17T07:46:37.000Z",
            "ImageId": "ami-000b4e0908095a643",
            "ImageLocation": "099720109477/ubuntu-minimal/images-testing/hvm-ssd/ubuntu-focal-daily-amd64-minimal-20211117",
            "ImageType": "machine",
            "Public": true,
            "OwnerId": "099720109477",
            "PlatformDetails": "Linux/UNIX",
            "UsageOperation": "RunInstances",
            "State": "available",
            "BlockDeviceMappings": [
                {
                    "DeviceName": "/dev/sda1",
                    "Ebs": {
                        "DeleteOnTermination": true,
                        "SnapshotId": "snap-0fc381f96849838fa",
                        "VolumeSize": 8,
                        "VolumeType": "gp2",
                        "Encrypted": false
                    }
                },
                {
                    "DeviceName": "/dev/sdc",
                    "VirtualName": "ephemeral1"
                },
                {
                    "DeviceName": "/dev/sdb",
                    "VirtualName": "ephemeral0"
                }
            ],
            "Description": "Canonical, Ubuntu Minimal, 20.04 LTS, UNSUPPORTED daily amd64 focal image build on 2021-11-17",
            "EnaSupport": true,
            "Hypervisor": "xen",
            "Name": "ubuntu-minimal/images-testing/hvm-ssd/ubuntu-focal-daily-amd64-minimal-20211117",
            "RootDeviceName": "/dev/sda1",
            "RootDeviceType": "ebs",
            "SriovNetSupport": "simple",
            "VirtualizationType": "hvm"
        }
    ]
}
